(function(){
	"use strict";
    let app = new App(getPuzzle());
    app.initialize();
    app.start();

    function getPuzzle(){
    	return [[0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,0,0,0,0,0]];
    }
})();

function App(puzzle){
	let app = this;
	app.initialize = initialize;
	app.start = start;
	app.sudokuSolver = undefined;
	app.sudokuSolver = undefined;

	function initialize(){
		app.viewController = new ViewController();
		app.sudokuSolver = new SudokuSolver(puzzle);
		app.sudokuSolver.setViewController(app.viewController);
		app.viewController.setSolver(app.sudokuSolver);
	}

	function start(){
		app.viewController.initialize();
		app.sudokuSolver.initialize();
	}
}

function ViewController(){

	let view = this;
	view.selectNumber = undefined;
	view.sudokuSolver = undefined;

	let _solveButton = $('#solve');
	let _resetButton = $('#reset');
	let _timeSlider = $('#time');
	let _number = $('.number');
	let _inputText = $('input[type="text"]');
	let _alert = $("#completeAlert");
	let _body = $('body');

	view.setSolver = setSolver;
	view.initialize = initialize;
	view.createBox = createBox;
	view.updateAlert = updateAlert;
	view.updateViewValue = updateViewValue;
	view.updatePuzzleValue = updatePuzzleValue;

	function setSolver(sudokuSolver){
		view.sudokuSolver = sudokuSolver;
	}

	function initialize(){
		view.updateAlert(false);
		_solveButton.click(solve);
		_resetButton.click(reset);
		_timeSlider.on('input', changeTime);
		$(document).click(documentClick);
		_number.click(selectNumber);
		view.sudokuSolver.updateBufferTime(_timeSlider.val());
		view.createBox();
	}

	function updateAlert(bool){
		if(bool){
			_alert.show();
		}else{
			_alert.hide();
		}
	}

	function solve(){
		view.sudokuSolver.solve();
		view.updateAlert(true);
	}

	function reset(){
		view.sudokuSolver.reset();
		view.updateAlert(false);
	}

	function changeTime(){
		view.sudokuSolver.updateBufferTime($(this).val());
	}

	function documentClick(){
		view.selectedNumber = undefined;
		_number.removeClass('active');
		_inputText.prop('readonly',false);
		_inputText.removeClass('dark');
		$('.inner-box>.row>.col').removeClass('dark');
		$('.outer-box>.col-sm-12').removeClass('dark');
		$('.outer-box').removeClass('dark');
		_body.removeClass('dark');
	}

	function selectNumber(event) {
		event.stopPropagation();
		_number.removeClass('active');
		$(this).addClass('active');
		view.selectedNumber = $(this).html();
		_inputText.prop('readonly',true);
		_inputText.addClass('dark');
		$('.inner-box>.row>.col').addClass('dark');
		$('.outer-box>.col-sm-12').addClass('dark');
		$('.outer-box').addClass('dark');
		_body.addClass('dark');
	}

	function inputClick(event){
		event.stopPropagation();
		if(view.selectedNumber){
			if(view.selectedNumber === 'X'){
				$(this).val('');
			}else{
				$(this).val(view.selectedNumber);
			}
			view.updatePuzzleValue();
		}
	}

	function updateViewValue(x,y,value){
		$('#puzzle'+x+y).val(value==0?'':value);
	}

	function updatePuzzleValue(){
		for(let x=0;x<9;x++){
			for(let y=0;y<9;y++	){
				view.sudokuSolver.updatePuzzleValue(x,y,$('#puzzle'+x+y).val());
			}
		}
	}


	function createBox(){
		for(let x=0;x<9;x++){
			let _row = $('<div>').addClass('col-sm-12');
			let _col = $('<div>').addClass("inner-box");
			let _innerRow = $('<div>').addClass('row');
			for(let y=0;y<9;y++){
				let _innerCol = $('<div>').addClass('col');
				let _input = $('<input>').addClass('form-control').attr('name','puzzle'+x+y).attr('id','puzzle'+x+y).attr('type','text');
				_innerRow.append(_innerCol.append(_input));
			}
			$(".outer-box").append(_row.append(_col.append(_innerRow)));
		}
		view.sudokuSolver.updateViewValue();
		_inputText = $('input[type="text"]');
		_inputText.click(inputClick);
		_inputText.keyup(view.updatePuzzleValue);
	}
}

function SudokuSolver(puzzle){

	let sudokuSolver = this;

	sudokuSolver.viewController = undefined;
	sudokuSolver.bufferTime = 0;

	sudokuSolver.setViewController = setViewController;
	sudokuSolver.initialize = initialize;
	sudokuSolver.solve = solve;
	sudokuSolver.possible = possible;
	sudokuSolver.reset = reset;
	sudokuSolver.isSolved = isSolved;
	sudokuSolver.updateBufferTime = updateBufferTime;
	sudokuSolver.updateViewValue = updateViewValue;
	sudokuSolver.updatePuzzleValue = updatePuzzleValue;

	function setViewController(viewController){
		sudokuSolver.viewController = viewController;
	}

	function initialize(viewController){
		sudokuSolver.updateViewValue();
	}

	async function solve(){
		for(let x=0;x<9;x++){
			for(let y=0;y<9;y++){
				if(puzzle[x][y]==0){
					for(let n=1;n<10;n++){
						if(sudokuSolver.possible(x,y,n)){
							puzzle[x][y]=n;
							sudokuSolver.updateViewValue();
							await new Promise(r => setTimeout(r, sudokuSolver.bufferTime));
							await sudokuSolver.solve();
							if(!sudokuSolver.isSolved()){
								puzzle[x][y]=0;
							}else{
								sudokuSolver.viewController.updateAlert(true);
								return;
							}
						}
					}
					return;
				}
			}
		}
	}

	function possible(x,y,n){
		for(let i=0;i<9;i++){
			if(puzzle[x][i]==n || puzzle[i][y]==n){
				return false;
			}
		}
		let x0 = Math.floor(x/3)*3;
		let y0 = Math.floor(y/3)*3;
		for(let i=0;i<3;i++){
			for(let j=0;j<3;j++){
				if(puzzle[x0+i][y0+j]==n){
					return false;
				}
			}
		}
		return true;
	}

	function reset(){
		for(let x=0;x<9;x++){
			for(let y=0;y<9;y++){
				puzzle[x][y]=0;
			}
		}
		sudokuSolver.updateViewValue();
	}

	function isSolved(){
		for(let x=0;x<9;x++){
			for(let y=0;y<9;y++){
				if(puzzle[x][y] == 0){
					return false;
				}
			}
		}
		return true;
	}

	function updateBufferTime(bufferTime){
		sudokuSolver.bufferTime = bufferTime;
	}

	function updatePuzzleValue(x,y,val){
		puzzle[x][y] = (val===''?0:val);
	}

	function updateViewValue(){
		for(let x=0;x<9;x++){
			for(let y=0;y<9;y++){
				sudokuSolver.viewController.updateViewValue(x,y,puzzle[x][y]);
			}
		}
	}
}